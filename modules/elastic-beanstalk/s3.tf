resource "aws_s3_bucket" "artifacts_bucket1554854565" {
  bucket = "${var.name}-beanstalk-artifacts"
  force_destroy = true

  tags = {
    Name        = "${var.name}-beanstalk-artifacts"
  }
}
