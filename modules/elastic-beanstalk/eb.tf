resource "aws_elastic_beanstalk_application" "eb_app" {
  name        = var.name
  description = "Application Created for ${var.name}"
}

resource "aws_elastic_beanstalk_environment" "eb_env" {
  name                = var.name
  application         = aws_elastic_beanstalk_application.eb_app.name
  solution_stack_name = var.app_env
  setting {
        namespace = "aws:autoscaling:launchconfiguration"
        name      = "IamInstanceProfile"
        value     = "aws-elasticbeanstalk-ec2-role"
      }
}

