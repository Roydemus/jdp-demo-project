# Variables used in the module

variable name {
  type        = string
  default     = ""
  description = "Name to be given on resources"
}

variable app_env {
  type        = string
  default     = "64bit Amazon Linux 2 v5.5.0 running Node.js 12"
  description = "Name of the environment of application"
}

