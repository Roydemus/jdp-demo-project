# Variables used in the module

variable "aws_region" {
  description = "The AWS region resources are created in"
  default     = "us-west-2"
}

variable "aws_profile" {

}

variable "name" {
  type        = string
  default     = ""
  description = "Name to be given on resources"
}

#Ref: https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.nodejs
variable "app_env" {
  type        = string
  default     = "64bit Amazon Linux 2 v5.5.0 running Node.js 12"
  description = "Name of the environment of application"
}