const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World! 3.0. Emmanuel ELOKA. Cloud Operations Engineer at JD Power')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
